Abstract: problem + contribution + method + results + impact

Introduction: problem + contribution

Methods: method (data source, data collection process, data preprocessing)

Results: results (table, graph) → can combine Discussion

Conclusion: impact/so what?/future work

# Abstract

Machines that exist alongside human activity must be capable of understanding
the effect that replacing steps in an imperative sequence has on attaining a
final result. To make progress toward this goal, we wrote a library and program
to automatically create possible replacements for recipe steps. Our method uses
state-of-the-art transformer models in order to cluster recipes with similar end
results and uses the spacy library to parse out sequence instructions and
rearrange them to form new instructions. From a subset of 600 recipes and 50
clusters, we were able to generate approximately 80,000 queries for possibly
swappable steps. Our library is a stepping stone for the training of models that
are capable of replacing and optimizing recipes with nuanced understanding about
novel choices.

# Introduction

- Helping Machines Reason About Steps and possible alternatives
- Problem to create a set of labelable queries to use to train a model.

- Defining swappable steps as two actions that, regardless of replacing one for
  the other, result in the same end state.
- Trying to make the library accessible through a python API and examples that
  demonstrate how to use it.

- Create a dataset.

# Methods

- This approach helps remove nonsensical substitutions.

- Used a food.com scraped dataset. (Chose 600 data points for the initial
  experiment).

N/K \* K^2 \* L^2

- Used Agglomerative Clustering

  - a 12:1 ratio of recipes to clusters to both minimize the size of the
    largest cluster and computation time. Complexity is O(NK).
  - So that we have the most likelihood of finding steps similar to one another.
  - SentenceTransformers framework (multi-qa-MiniLM-L6-cos-v1 model due to its
    ability to generalize and its generation of normalized vectors for
    optimized computations)

- Step Parsing

  - Used spacy's en_core_web_trf model to parse sentences out.
  - Algorithmically figured out what the main verb, the direct objects, and the
    independent clauses were. (separates complementary verbs and compound
    sentences through various strategies)

- Comparing between two different recipes

  - The first recipe is the reference (R), and the second is the query generator
    (Q).
  - For each pair of steps $R\_i$ and $Q\_j$
  - If the main verbs have sufficient similarity (but not identical, between 0.4
    and 0.81), create a new step where $R\_i$'s main verb is substituted by
    $Q_j$'s main verb.
  - If any of the direct objects have sufficient similarity, we go to a
    matchmaking algorithm in which we figure out what direct object of $R\_i$
    matches with another direct object of $Q\_j$.

- Matchmaking
  - Each direct object in $R\_i$ picks two candidates for $Q\_j$.
  - $Q\_j$ then picks the best (most similar) candidate out of the ones from
    $R\_i$. Direct objects of $R\_i$ prune all candidates except one, then
    direct objects from $Q\_j$ know what to swap with.

# Results

- Discuss the shape of the JSON (ingredients not included because it could
  indicate that a step was not in the recipe), structured as (first recipe,
  second recipe, queries)
- 79,000 queries created from just 600 recipes (7 hours to do so b/c
  multithreading issues, but maybe we don't include this).
- SHA256 Hashes of the clusters used to prevent overwriting.

- Maybe show an example (a subtle change between mushrooms and dried mushrooms)
- Imperative sequence of instructions.
- At the current moment, it relies a bit too much on the relatively
  deterministic pattern that recipe steps follow (more complex and implicit
  methods will currently not be recognized and simply ignored)

# Conclusion

- Developed a library that can be used to generate possibly swappable datasets
- Applications in human imperative sequences: Optimizing out human-to-human
  interactions and helping make steps that might have been made more inefficient
  by human error.
