# Abstract

Any action has an intended start and end state. Helping machines understand
which events are swappable would also allow them to extract some reasoning about
the relationship between the starting state and the action that it goes through.
Currently existing models have difficulty reasoning about these events
because of the highly variable and context-sensitive nature of an action's
effect. To make progress toward understanding events, we focused on recipes,
which provide consistent sequences of steps. We present a seed query database
containing possibly swappable steps for recipes and a generalizable library to
automatically generate new datasets from other instruction-based text. Our
method clusters SentenceTransformers embeddings created from recipe titles so
that recipes with similar end states are grouped together. Inside these
clusters, we generate possible replacements from recipe steps that share a
marginal amount of similarity. From a subset of 600 recipes and 50 clusters, we
are able to generate approximately 80,000 queries. Our library is a stepping
stone for the future development of models that are capable of modifying
instructions without significantly changing final results.

# Introduction

Understanding events more.

- Understanding events more → talk about challenges with understanding
  events/procedural text
- Helping Machines Reason About Steps and possible alternatives
- Problem to create a set of labelable queries to use to train a model.

- Defining swappable steps as two actions that, regardless of replacing one for
  the other, result in the same end state.
- Trying to make the library accessible through an API and examples that
  demonstrate how to use it.

- Create a seed dataset to demonstrate the viability of the library.

# Related Work

- Work done in Wikihow and reasoning there
- Possible work in recipes
- Possible other datasets? → datasets/prior work for event understanding,
  procedural text, script learning

# Methods

- make it as simple as possible. (Clarity Purposes)

- This approach helps remove nonsensical substitutions and preserves the
  original structure of the sentence.
- Uses comparison to make sure that it's similar enough, but not TOO similar.
  (include range)

- Used a food.com scraped dataset. (Chose 600 data points for the initial
  experiment).

- Used Agglomerative Clustering

  - a 12:1 ratio of recipes to clusters to both minimize the size of the largest
    cluster and computation time.
  - So that we have the most likelihood of finding steps similar to one another.
  - SentenceTransformers framework (multi-qa-MiniLM-L6-cos-v1 model due to its
    ability to generalize and its generation of normalized vectors for optimized
    computations)

- Step Parsing

  - Used spacy's en_core_web_trf model to parse sentences out.
  - Algorithmically figured out what the main verb, the direct objects, and the
    independent clauses were. (separates complementary verbs and compound
    sentences through various strategies)

- Comparing between two different recipes

  - The first recipe is the reference (R), and the second is the query generator
    (Q).
  - For each pair of steps $R\_i$ and $Q\_j$
  - If the main verbs have sufficient similarity (but not identical, between 0.4
    and 0.81), create a new step where $R\_i$'s main verb is substituted by
    $Q_j$'s main verb.
  - If any of the direct objects have sufficient similarity, we go to a
    matchmaking algorithm in which we figure out what direct object of $R\_i$
    matches with another direct object of $Q\_j$.

- Matchmaking
  - Each direct object in $R\_i$ picks two candidates for $Q\_j$.
  - $Q\_j$ then picks the best (most similar) candidate out of the ones from
    $R\_i$. Direct objects of $R\_i$ prune all candidates except one, then
    direct objects from $Q\_j$ know what to swap with.

# Results

- Discuss the basic shape of the JSON (contains A, B, C) (ingredients not
  included because it could indicate that a step was not in the recipe),
  structured as (first recipe, second recipe, queries)
- 79,000 queries created from just 600 recipes

- Maybe show an example (a subtle change between mushrooms and dried mushrooms)
- Imperative sequence of instructions.

- Examples of stuff that might be too similar, or just not related.

- Future work - parsing the non-deterministic pattern that other instructions
  might have.
- At the current moment, it relies a bit too much on the relatively
  deterministic pattern that recipe steps follow (more complex and implicit
  methods will currently not be recognized and simply ignored)

# Conclusion

- Developed a library that can be used to generate possibly swappable datasets
- Applications in human imperative sequences: Models can replace steps with the
  knowledge that these changes will not change the end result too much.
- Changing instructions, modifying them, understanding them, answering questions
  about instructions. (may even be able to understand stuff that isn't
  imperative, e.g. stories, news, …)
