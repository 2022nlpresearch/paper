\documentclass[12pt]{article}

\usepackage{amsmath}
\usepackage{hyperref}
\usepackage[margin=1in]{geometry}

\usepackage[style=apa,backend=biber]{biblatex}
\addbibresource{main.bib}

\title{Query Generators for Datasets of Interchangeable Recipe Steps}
\author{Juni Kim}

\author{
  Juni Kim\\
  Stanford Online High School\\
  \texttt{junickim@ohs.stanford.edu}
  \and
  Alyssa Hwang\\
  University of Pennsylvania\\
  \texttt{ahwang16@seas.upenn.edu}
}

\date{\today}

\begin{document}

\maketitle

\begin{abstract}
  Any action has an intended start and end state. Helping machines understand
  which events are swappable would also allow them to extract some reasoning about
  the relationship between the starting state and the action that it goes through.
  Currently existing models have difficulty reasoning about these events
  because of the highly variable and context-sensitive nature of an action's
  effect. To make progress toward understanding events, we focused on recipes,
  which provide consistent sequences of steps. We present a seed query database
  containing possibly swappable steps for recipes and a generalizable library to
  automatically generate new datasets from other instruction-based text. Our
  method clusters SentenceTransformers embeddings created from recipe titles so
  that recipes with similar end states are grouped together. Inside these
  clusters, we generate possible replacements from recipe steps that share a
  marginal amount of similarity. From a subset of 600 recipes and 50 clusters, we
  are able to generate approximately 80,000 queries. Our library is a stepping
  stone for the future development of models that are capable of modifying
  instructions without significantly changing final results.
\end{abstract}


\section{Introduction}

An event consists of an action which modifies some state, while procedural text
aims to describe a sequence of events to be executed in series. In our
investigation, we consider pairs of \textit{swappable events}, where replacing
one of the two events with the other results in an equivalent (or close to
equivalent) end state.

Let there be a finite sequence of events $A = a_1, a_2, \ldots a_n$, where each event $a_i$ is
encoded as a sentence. We then define a \textit{query} as a possible replacement $a_i'$
for $a_i$ such that the sequences $A$ and $a_1, \ldots
a_{i}', \ldots a_n$ may or may not yield the same end states from the same
initial state.

In order to train a classifier for detecting the swappability of two events,
a dataset of queries for each recipe must be compiled and labeled. However, if we were to
use an event from a different event sequence $b_j \in B$ (Where $B$ is
also an event sequence) and let it be a query for $a_i$, it would almost certainly
violate much of the context of the original sequence $A$, which may include certain
assumed skills or ingredients that one possesses. In the labeling phase,
virtually all queries would be labeled as not swappable with the step they are
trying to replace.

This paper thus presents a library for generating queries that preserve the
structure of present recipe steps, along with a seed database. Recipes were
selected because they tend to have a well-defined imperative structure and
purpose.

\section{Related Work}

\subsection{State Tracking}

State tracking allows a model to understand how the state of the world changes
as a given series of text occurs. \cite{henderson_thomson_young_2014} uses a
recurrent neural network architecture to track state as a dialogue goes on,
while \cite{state_dialog} uses an attention-based method and newer word
embeddings.

Recurrent neural networks have also been used to extract events from text, as in
\cite{nguyen_cho_grishman_2016}.

\subsection{Procedural Reasoning}

Researchers have been working on ways to help machines understand the relations
that various steps have with each other. \cite{zhang_lyu_callison-burch_2020}
has used Wikihow as a resource in order to generate datasets (with improved
model results) that attempt to
understand the steps that are required for a goal, as well as the ordering that
steps must have in respect with each other. Furthermore,
\cite{zhou_zhang_yang_lyu_yin_callison-burch_neubig_2022} constructs a knowledge
base of wikihow steps which understands step dependencies and contains links
between steps from different articles.

Our research aims to not only link steps between recipes, but to use them to
generate new possible replacements that still make grammatical and contextual sense.

\section{Methods}

\subsection{SentenceTransformers}

The SentenceTransformers framework, created by \cite{sbert}, uses pre-trained
state-of-the-art BERT transformer models in order to convert phrases or
instructions into a vector embedding encoding the meaning of its input.

We extensively use these encodings in order to calculate the cosine similarity
between any two phrases, which represents how close they are semantically.

As a result, we decide to use the multi-qa-MiniLM-L6-cos-v1 model, because it
is generalizable and creates normalized embeddings, allowing us to optimize
cosine similarity computations into dot products (\cite{sbert})
model.

\subsection{Clustering}

Our library first assumes that the dataset will have recipes with both a title
and a list of recipe steps. Our library uses the SentenceTransformers framework
from \cite{sbert} in order to first turn all recipe titles into vector embeddings.

It then uses Agglomerative Clustering, implemented by \cite{sklearn}, in order
to recursively cluster title embeddings using cosine similarity as the linkage
distance. This method ensures that recipes with similar titles (which is a good
metric for the result, or end-state, of a recipe) are compared, giving us the
greatest chance of finding two recipes with roughly similar steps.

\subsection{Parsing}

Once clustering has finished, the algorithm begins to parse each recipe to
extract its steps. Since most recipe steps are imperative commands with a
specified direct object and possibly some prepositions, these were our primary
focus of parsing.

First, all sentences in a recipe step are split from each other. Using the spaCy
library's en\_core\_web\_trf model (which prioritizes accuracy), these sentences
are then parsed out into a tree of tokens.

Second, these sentences are split into independent clauses, detected by
coordinating conjunctions over verbs.

Finally, each independent clause is traversed (via the spaCy-generated tree) to
find the main verb, direct objects, and prepositional clauses (which may
indicate position, time, etc.), which are important for maintaining the context
of newly created instructions. If the verb is empty or a stop word, or all
direct objects are stop words, the clause is considered unparseable and ignored.

\subsection{Query Generation}

Queries are generated pairwise between parseable steps from different recipes.

Let us consider the main verb, the set of direct objects, and prepositional
clauses of our first parseable step as $V_0, D_0, P_0$ respectively. We will
also use the same labeling scheme for the second parseable step, which is from a
different recipe: $V_1, D_1, P_1$.

If the cosine similarity of $V_0$ and $V_1$ (determined by the
SentenceTransformers encoder) is between 0.40 and 0.83, we generate a new query
where $V = V_1, D = D_0, P = P_0$. We choose this boundary because we want to
find verb pairs that have a reasonable amount of similarity, but not to the point
where they are essentially exact copies.

If $\exists a \in D_0, b \in D_1 \ni 0.40 < cos\_sim(a, b) < 0.83$, then we will
construct a new set $D_0'$ by starting with $D_0$ and replacing as many elements
as possible with analogues from $D_1$ by using the criteria described before. We
then generate a new query where $V = V_0, D = D_0', P=P_0$.

If both conditions above are satisfied, then we  generate a new query where $V =
V_1, D = D_0', P=P_0$.


\subsubsection{Generating $D_0'$}

Each element $d$ of $D_0$ selects at most two candidates from $D_1$ that satisfy
the similarity metric. Each element $D_1$, from the elements of $D_0$
that selected it as a candidate, only accepts the one with the greatest
similarity to it. Each element of $D_0$ is then replaced by a candidate from $D_1$
which had accepted it with the greatest similarity to itself, if such an element
exists.


\section{Results}

The library, and our experimental code, are available at
\url{https://gitlab.com/2022nlpresearch/dsgen}.

Our algorithm was able to generate approximately 80,000 queries from a subset of
600 recipes from \cite{foodcom}. In order to expand the number of queryable
recipes, we can randomly remove pairs of recipes or their steps from inspection.

\subsection{Clause/Query Representation}
\label{subsection:representation}

Since we had initially confirmed the structure of any parseable clause, its
sentence representation is reconstructed in the following order:

\begin{enumerate}
  \item main verb
  \item direct objects, separated with commas and ``ands"
  \item all other prepositional clauses in sequential order.
\end{enumerate}

If a clause is not parseable, it is just represented in its original form.

\subsection{Data Format}

The seed database contains a collection of JSON files which contain the two
recipes that were analyzed for queries, and a list of pairs of an original step
(always from the first recipe) and an algorithmically generated step that is
possibly swappable with the first. When printing the steps of a recipe, all
clauses that can be reconstructed as stated in
\autoref{subsection:representation} are printed in that format, while clauses
that cannot retain their original form. This ensures no significant grammatical
deviations between an original step and a replacement.

\subsection{An Example}

Taken from two recipes, the first being a recipe on breakfast pizza, and the other being
chinese chop suey, we extract the following queries for replacing steps of the
breakfast pizza:

\begin{itemize}
\item "cut sausage into small pieces", "slice sausage into small pieces"
\item "cut sausage into small pieces", "reduce sausage into small pieces"
\item "whisk eggs, and milk in a bowl until frothy", "whisk eggs, and  cup of soy sauce in a bowl until frothy"
\end{itemize}

Note that these queries still retain grammatical structure, despite some of the
verbs and objects being replaced.

\subsection{Future Work}

Future progress will involve trying to generalize this parsing algorithm so that
it is able to understand imperative structures that are more subtle and
indirect. This is especially the case in text other than recipes, where commands
may merge with other clauses.

\section{Conclusion}

We present a library that can algorithmically generate possible replacements for
recipe steps while preserving grammar and contextual information. Our library
has applications for models that are capable of analyzing and understanding
imperative sequences of text. These models be able to replace steps with the
knowledge that the changes that they make will not change the end result or to a
miniscule degree, and even answer questions about said steps. Future extensions
of our work may also apply to more irregular, non-recipe text, such as stories
or news.

\printbibliography

\end{document}
