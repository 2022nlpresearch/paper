# Paper

### Compilation with TeXbld

This paper is built with TeXbld.

1. cd into the project directory.
2. ```sh
   texbld build
   texbld run compile
   ```

### Compilation on a normal system

run `latexmk -pdf`
